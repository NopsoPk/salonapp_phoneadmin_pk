import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { config } from './config';


@Injectable()
export class AppData {

  constructor(private http: Http) {
    /// console.log('Hello AppData Provider');
  }

  getSalons(params) {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    const options = new RequestOptions({ headers: headers });
    const jsonData = JSON.stringify(params)
    const percentEncodedJsonData = encodeURIComponent(jsonData)
    console.log(config.ApiUrl + percentEncodedJsonData)
    return this.http.post(config.ApiUrl + percentEncodedJsonData, options)
      .map(Response => Response.json())
  }

  getPrevApp(params) {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    const options = new RequestOptions({ headers: headers });
    const jsonData = JSON.stringify(params)
    const percentEncodedJsonData = encodeURIComponent(jsonData)
    console.log(config.ApiUrl + percentEncodedJsonData)
    return this.http.get(config.ApiUrl + percentEncodedJsonData, options)
      .map(Response => Response.json())
  }

  startAppt(params) {
    // console.log(comment);
    // console.log(id);
    // const token = this.getToken();
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({ headers: headers });
    const jsonData = JSON.stringify(params)
    const percentEncodedJsonData = encodeURIComponent(jsonData)
    console.log('final Base URL');
    console.log(config.ApiUrl + percentEncodedJsonData);


    return this.http.post(config.ApiUrl + percentEncodedJsonData, options)
      .map((res) => res.json())
  }

  removeAppt(params) {
    // console.log(comment);
    // console.log(id);
    // const token = this.getToken();
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({ headers: headers });
    const jsonData = JSON.stringify(params)
    const percentEncodedJsonData = encodeURIComponent(jsonData)
    return this.http.post(config.ApiUrl + percentEncodedJsonData, options)
      .map((res) => res.json())
  }

  rescheduleAppt(params) {
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    const options = new RequestOptions({ headers: headers });
    const jsonData = JSON.stringify(params)
    const percentEncodedJsonData = encodeURIComponent(jsonData)
    console.log(config.ApiUrl + percentEncodedJsonData)
    return this.http.get(config.ApiUrl + percentEncodedJsonData, options)
      .map(Response => Response.json())
  }

  finishAppt(params) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const options = new RequestOptions({ headers: headers });
    const jsonData = JSON.stringify(params)
    const percentEncodedJsonData = encodeURIComponent(jsonData)

    return this.http.post(config.ApiUrl + percentEncodedJsonData, options)
      .map((res) => res.json())
  }


  getToken() {
    return localStorage.getItem('token');
  }

  setToken(token) {
    localStorage.setItem('token', token);
  }

  removeToken() {
    localStorage.removeItem('token');
  }
}
