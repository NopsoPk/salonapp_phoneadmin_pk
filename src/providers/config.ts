export function getBaseURL(): string {
  let apiMode = Number(localStorage.getItem('zahidzahidzahid'))
  console.log('api mode is : ' + apiMode);
  // apiMode = 2
  switch (apiMode) {
    case 1:
      return 'https://www.beautyapp.pk/apis_pk/'
    case 2:
      return 'https://nopso.qwpcorp.com/apis_pk/'
    case 3:
      return 'https://nopso.qwpcorp.com/apis_pk/Shary'
    default:
      return 'https://www.beautyapp.pk/apis_pk/'

  }
}

export const Base = {
  url: getBaseURL(),
};
export const config = {
  'ApiUrl': Base.url + 'jsona59.php?json=', // 'http://nopso.qwpcorp.com/apis_pk/jsona59.php?json=',
  'custImg': Base.url + 'customers/',
  'techImg': Base.url + 'technicians/',
  'salonImgUrl': Base.url + 'salonimages/',
}