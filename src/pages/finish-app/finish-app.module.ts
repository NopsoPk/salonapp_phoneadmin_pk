import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinishAppPage } from './finish-app';

@NgModule({
  declarations: [
    FinishAppPage,
  ],
  imports: [
    IonicPageModule.forChild(FinishAppPage),
  ],
})
export class FinishAppPageModule {}
