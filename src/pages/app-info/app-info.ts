import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { NativeTransitionOptions, NativePageTransitions } from '@ionic-native/native-page-transitions';


import { ViewController } from 'ionic-angular/navigation/view-controller';

import { Storage } from '@ionic/storage';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GlobalProvider } from '../../providers/global';
import { TabsPage } from '../tabs/tabs';
import { Geolocation } from '@ionic-native/geolocation';
import { LoginPage } from '../login/login';



@IonicPage()
@Component({
  selector: 'page-app-info',
  templateUrl: 'app-info.html',
})
export class AppInfoPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public nativePageTransitions: NativePageTransitions,
    public global: GlobalProvider,
    public viewCtrl: ViewController,
    public storage: Storage,
    public splashScreen: SplashScreen

  ) {
  }
  selectedEnviroment: number = 1
  ionViewDidLoad() {
    this.selectedEnviroment = this.global.getFromLocalStorage('zahidzahidzahid')
    if (!this.selectedEnviroment) this.selectedEnviroment = 1

    console.log('ionViewDidLoad AppInfoPage');
  }
  btnBackTapped() {
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 0
    };

    if (this.platform.is(`ios`)) this.nativePageTransitions.slide(options)

    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    } else {
      this.navCtrl.setRoot(TabsPage)
    }
  }

  switchEnviroment(environment: number) {
    this.selectedEnviroment = environment
    
    switch (environment) {
      case 1:  // this means enviroment is connect with live api
        this.global.setInLocalStorage('zahidzahidzahid', 2)
        this.viewCtrl.dismiss().then(() => {
          this.onSuccess()
        });
        break;
      case 2: // this means enviroment is connect with Dev Mode
        this.global.setInLocalStorage('zahidzahidzahid', 1)
        this.viewCtrl.dismiss().then(() => {

          this.onSuccess()

        });
        break;
      default:
        this.global.setInLocalStorage('zahidzahidzahid', 1)
        this.viewCtrl.dismiss().then(() => {
          this.onSuccess()
        });
        break;
    }
  }
  onERror() {

  }
  onSuccess() {
    localStorage.clear()
    
    this.splashScreen.show()
    window.location.reload();

    this.navCtrl.setRoot(LoginPage).then(() => {
      this.global.setInLocalStorage('zahidzahidzahid', this.selectedEnviroment)
    })
  }
  btnDismissTappe() {
    this.viewCtrl.dismiss()
  }
}
