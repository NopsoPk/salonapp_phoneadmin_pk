import { Component } from '@angular/core';

import { DashboardPage } from '../dashboard/dashboard'
import { AppointmentsPage } from '../appointments/appointments'
import { NotificationsPage } from '../notifications/notifications'

import { SettingsPage } from '../settings/settings';
import { NavController, NavParams, Events } from 'ionic-angular';
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  public setTabIndex: number
  // selectedInex = 4;
  tab1Root = DashboardPage;
  tab2Root = AppointmentsPage;
  tab3Root = NotificationsPage;
  tab4Root = SettingsPage;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,

  ) {
    this.setTabIndex = navParams.get('opentab');
    if (!this.setTabIndex) this.setTabIndex = 0
    this.events.subscribe('setTabIndex', gotTabIndex => {
      // this.setTabIndex = gotTabIndex
      // alert('gotTabIndex '+gotTabIndex)
    })

  }

  public hello() {
    // alert('sdfsd');
    // this.navCtrl.setRoot(this.tab2Root);
  }

}
