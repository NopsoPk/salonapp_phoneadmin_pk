import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Renderer } from '@angular/core';
import { Platform } from 'ionic-angular/platform/platform';

/**
 * Generated class for the NotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notes',
  templateUrl: 'notes.html',
})
export class NotesPage {
  public unregisterBackButtonAction: any;
  notes: string;
  

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public renderer: Renderer,
    private platform: Platform,
  ) {
      this.notes = navParams.get('notes');
     
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotesPage');
  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  closemodal() {
    // this.notes = '';
    this.viewCtrl.dismiss(this.notes);
    console.log('notes', this.notes);
  }

  savenotes() {
    this.viewCtrl.dismiss(this.notes);
    console.log('notes', this.notes);
  }


  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.navCtrl.pop();
    }, 10);
  }
  //end custom back button for android


}
