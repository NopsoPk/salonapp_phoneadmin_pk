import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Navbar, Platform } from 'ionic-angular';
import { ServiceManager } from '../../providers/service-manager'
import { GlobalProvider } from '../../providers/global'
import { SharedataProvider } from '../../providers/sharedata/sharedata'
import { Tech_Slots, Sal_Techs, Tech_appointments } from '../../providers/salonInterface'
import { AppVersion } from '@ionic-native/app-version';
import { Events } from 'ionic-angular';

import 'rxjs/add/operator/retrywhen';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/scan';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { AppMessages } from '../../providers/AppMessages/AppMessages';

@IonicPage()
@Component({
  selector: 'page-slots-view',
  templateUrl: 'slots-view.html',
})
export class SlotsViewPage {
  @ViewChild(Navbar) navBar: Navbar;

  public unregisterBackButtonAction: any;
  public calendarOption;
  selectedDateWithMonthName: string
  appStartDateString: string
  staticCalendar = []
  dateComponent = []

  public selectedDate: any
  appointmentDate: string;
  techSlots: Tech_Slots[];

  selected_Slot: Tech_Slots
  selectedSlotIndex = 0
  selectedDateIndex = 0
  monthNames = [
    'January', 'February', 'March',
    'April', 'May', 'June',
    'July', 'August', 'September',
    'October', 'November', 'December'
  ];
  // # Nav Params
  selected_Tech: Sal_Techs
  appo_TotalTime: Number
  appo_TotalPrice: Number
  appo_Services: any[]
  appoServicesString: string
  cust_id: string
  isReschedule = false
  isMakeAppointment = false;
  appointment: Tech_appointments
  selected_TechIndex: number;
  isRecheduleFromNotifications: boolean
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public serviceManager: ServiceManager,
    public global: GlobalProvider,
    private sharedProvider: SharedataProvider,
    public appVersion: AppVersion,
    public loadingCtrl: LoadingController,
    public events: Events,
    public nativePageTransitions: NativePageTransitions,
    private platform: Platform,
  ) {
    this.techSlots = []
    console.log('isReschedule : ' + this.isReschedule);
    this.appo_TotalTime = navParams.get('appo_TotalTime');
    this.appo_TotalPrice = navParams.get('appo_TotalPrice');
    this.appo_Services = navParams.get('appo_Services');
    this.cust_id = navParams.get('cust_id');
    this.selected_Tech = navParams.get('selected_Tech');
    this.selected_TechIndex = navParams.get('selected_TechIndex');
    this.appoServicesString = navParams.get('appoServicesString');
    this.isReschedule = navParams.get('isReschedule');
    if (this.isReschedule) {
      this.isReschedule = navParams.get('isReschedule');
      this.appointment = navParams.get('appointment');
      this.isRecheduleFromNotifications = navParams.get('isRecheduleFromNotifications');
    }
    console.log(this.global.getFromLocalStorage(this.global.SAL_FUTURE_APP_DAYS));
    let future = this.global.getFromLocalStorage(this.global.SAL_FUTURE_APP_DAYS);

    this.calendarOption = {
      fontSize: 4.23,
      fontWeight: 600,
      numberOfDays: future,
      selectedColor: 'black',
      defaultColor: 'white',
      circleBorderColor: '1px solid #969696',
      selectedDateColor: 'white',
      seletedDayColor: 'white',
      selectedMonthAndYearColor: 'rgba(255,255,255,0.5)',
      unSelectedDateColor: 'rgb(150,150,150)',
      circleBackgroundColor: 'transparent',
      hieghtOfCalendar: '140px',
      calendarBackgroundColor: 'transparent',
      sizeOfCircle: 6.5
    };
  }


  dateDidSelect(date, index) {
    console.log('ddddate', date);

    const d = new Date();
    if (Number(date.toString()) <= 9) {
      date = '0' + date.toString();
    }
    console.log('selected Date is: ' + date);
    console.log('The current month is' + this.monthNames[d.getMonth()]);
    this.selectedDateWithMonthName = date + ' ' + this.monthNames[d.getMonth()]
    this.selectedDateIndex = index
    this.dateComponent[2] = date

    this.appStartDateString = this.dateComponent[0] + '-' + this.dateComponent[1] + '-' + this.dateComponent[2]
    console.log('_appStartTime', this.appStartDateString);

    const dateParsing = Date.parse(this.appStartDateString)
    if (!dateParsing) {
      this.global.makeToastOnFailure(AppMessages.msgDateParsingError + this.appStartDateString)
      return
    }

    this.getTechSlots(this.appStartDateString)
  }

  ionViewCanEnter() {
    console.log('can enter');
  }

  ionViewWillEnter() {
    const todayDate = this.global.getCurrentDeviceDate('')
    this.selectedDate = new Date();
    console.log('this.selectedDate', this.selectedDate);

    this.appStartDateString = todayDate
    console.log('today date', todayDate);
    this.selectedDateWithMonthName = this.global.AvailableSlotsFor(todayDate.toString())
    this.getTechSlots(todayDate)
  }


  ionViewDidLoad() {

    console.log('ionViewDidLoad SlotsViewPage');
    this.navBar.backButtonClick = (e: UIEvent) => {
      console.log('hehehe');
      let options: NativeTransitionOptions = {
        direction: 'right',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 0
      };
      this.nativePageTransitions.slide(options)
      if (this.navCtrl.canGoBack()) {
        this.navCtrl.pop({
          animate: false,
          animation: 'ios-transition',
          direction: 'back',
          duration: 500,
        })
      }

    }
  }
  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  // @IBACTIONS
  btnBookAppointmentTapped() {
    console.log('isReschedule : ' + this.isReschedule);
    if (this.selected_Slot === undefined) {
      this.global.makeToastOnFailure(AppMessages.msgSelectTimeSlot)
      return;
    }
    // const _appStartTime = this.global.
    console.log('app start time to Book Appointment', this.selectedDate);

    // if (this.selectedDate !== undefined) {
    this.appointmentDate = this.global.getCurrentDeviceDate(this.selectedDate.toString());
    // } else {
    // this.appointmentDate =  this.selectedDate.toString();
    // }
    const appStartTime = this.appointmentDate + ' ' + this.selected_Slot.ts_start_time.toString().trim() + ':00'

    console.log('app start time to Book Appointment', appStartTime);
    let service = ''
    let app_id = ''
    // let userInfo = this.getAppVersion()
    if (this.isReschedule) {
      service = 'update_appointment'
      app_id = this.appointment.app_id
    } else {
      service = 'make_appointment'
      app_id = ''
    }


    // alert('userInfo: ' + userInfo)


    const params = {
      service: btoa(service.toString().trim()),
      app_status: btoa(this.global.APP_STATUS_ACCEPTED.toString()).trim(),
      app_type: btoa('appointment'.trim()), // this string is as it is. not know whats benefit of it
      sal_id: btoa(this.global.getLoggedInSalonID().toString().trim()),
      cust_id: btoa(this.cust_id.toString().trim()),
      tech_id: btoa(this.selected_Tech.tech_id.toString().trim()),
      app_id: btoa(app_id.toString().trim()),
      app_services: btoa(this.appoServicesString.trim()),
      app_price: btoa(this.appo_TotalPrice.toString().trim()),
      app_est_duration: btoa(this.appo_TotalTime.toString().trim()),
      app_start_time: btoa(appStartTime.trim()),
      app_slots: btoa(this.selected_Slot.ts_number.toString().trim()),
      app_user_info: btoa('appName:phone-admin-ionic,version: 1.0.0'), // version info build,api,version etc
      device_datetime: btoa(this.global.getCurrentDeviceDateTime().trim()),
    }
    //
    console.log('params are', params);
    this.global.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe((res) => {
        console.log('response from appo');
        console.log(res);
        this.global.stopProgress()

        if (res['status'] === '0') {

          this.global.makeToastOnFailure(res.error)

          return
        }
        if (res['status'] === '1') {
          if (this.isReschedule) {
            console.log('response for update appointment', res);

            console.log('appointment updated successfully.');
            this.global.makeToastOnSuccess(AppMessages.msgAppointmentUpdation)
            if (this.isRecheduleFromNotifications) {
              this.sharedProvider.updateNotificationsData.next(true)
            }
            // this.events.publish('NeedToUpdate', true);
          } else {
            console.log('appointment added successfully.');
            this.global.makeToastOnSuccess(AppMessages.msgAppointmentAddition)
          }
          this.isMakeAppointment = true;
          this.sharedProvider.makeAppointmentSource.next(this.isMakeAppointment);
          this.sharedProvider.techIndex.next(this.selected_TechIndex);

          // this.sharedProvider.techAppointmentComplete.next(this.selected_Tech);

          this.navCtrl.popToRoot().then(() => {
            this.navCtrl.getActive(true).didEnter.subscribe(() => {
              this.navCtrl.getActive().data.zahid = true;

            })
          })
          // this.navCtrl.push(MenuPage, {})
        } else {
          this.global.makeToastOnFailure(AppMessages.msgAppointmentFailure)
        }
        console.log('response of book appointment', res);

      }, error => {

        this.global.stopProgress()
        // this.global.makeToastOnFailure(error, 'top')
        this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        console.log('something went wrong', error);
      })
  }


  // CUSTOME FUCNTIONS
  getAppVersion(): string {
    let appInfo = ''
    this.appVersion.getAppName()
      .then(appName => {
        appInfo += 'app_Name' + appName + ','
      })
    this.appVersion.getPackageName().then(packageName => {
      appInfo += ('package_Name' + packageName + ',').trim()
    })

    this.appVersion.getVersionCode().then(versionCode => {
      appInfo += ('version_Code' + versionCode + ',').trim()
    })
    this.appVersion.getVersionNumber().then(versionNumber => {
      appInfo += ('versionNumber' + versionNumber + ',').trim()
    })
    return appInfo
  }
  selectedSlot(selectedSlot, index) {
    this.selectedSlotIndex = index;
    this.selected_Slot = selectedSlot
    console.log('selectedSlot', this.selected_Slot);

  }
  public getTechSlots(date: any) {
    const datestr = this.global.getCurrentDeviceDate(date.toString())
    let appID = ''
    if (this.isReschedule) {
      appID = this.appointment.app_id.toString().trim()
    }
    const params = {
      service: btoa('get_tech_slots'),
      tech_id: btoa(this.selected_Tech.tech_id.toString().trim()),
      app_id: btoa(appID),
      app_duration: btoa(this.appo_TotalTime.toString().trim()),
      app_date: btoa(datestr.trim()),
      device_datetime: btoa(this.global.getCurrentDeviceDateTime()),
    }

    console.log('params: get tech slots ', params);


    this.global.showProgress()
    this.serviceManager.getData(params)
      .retryWhen((err) => {
        return err.scan((retryCount) => {
          retryCount += 1;
          if (retryCount < 3) {
            return retryCount;
          }
          else {
            throw (err);
          }
        }, 0).delay(1000)
      })
      .subscribe(res => {
        this.techSlots = []
        this.global.stopProgress()
        console.log('techSlots', res);
        if (res.status === '0') {
          this.global.makeToastOnFailure(res.error);
          return;
        }
        if (res.tech_slots) {
          this.techSlots = res.tech_slots
        }
        if (this.techSlots[this.selectedSlotIndex] !== undefined) {
          let slot = this.techSlots[this.selectedSlotIndex]
          console.log('__selected slot', slot);
          this.selected_Slot = slot
        } else {
          console.log('no tech slot in will enter');

        }
      }, error => {

        this.global.stopProgress()
        // this.global.makeToastOnFailure(error, 'top')
        this.global.makeToastOnFailure(AppMessages.msgNoInternetAVailable)
        console.log('something went wrong', error);
      }
      );
  }


  calculateAppSlots(timeStr: string): string {
    let slotsString = ''
    const totalTime = parseInt(timeStr, 10)
    if (totalTime === undefined) {
      console.log('is undefined');
      return
    }

    let numberOfSlots = totalTime / 5
    if (totalTime % 5 !== 0) {
      numberOfSlots = Math.ceil(numberOfSlots)
    }
    console.log('num of slots ', numberOfSlots);

    const ts_slotNo = parseInt(this.selected_Slot.ts_number, 10)


    for (let index = 0; index < numberOfSlots; index++) {
      slotsString += ts_slotNo + index + ','
    }
    slotsString = slotsString.slice(0, -1)

    console.log('slots string: ', slotsString);

    return slotsString
  }
  convertDateTo24Hour(date) {
    const elem = date.split(' ');
    const stSplit = elem[1].split(':'); // alert(stSplit);
    let stHour = stSplit[0];
    const stMin = stSplit[1];
    const stAmPm = elem[2];
    // alert("hour:"+stHour+"\nmin:"+stMin+"\nampm:"+stAmPm); //see current values

    if (stAmPm === 'PM') {
      if (stHour !== 12) {
        stHour = stHour * 1 + 12;
      }

    } else if (stAmPm === 'AM' && stHour === '12') {
      stHour = stHour - 12;
    } else {
      stHour = stHour;
    }

    return elem[0] + ' ' + stHour + ':' + stMin;
  }
  // getSlotFromattedTime(slotTime) {
  //   console.log('app_start_time: ' + slotTime);
  //   return this.global.getFormattedTime(slotTime);
  // }

  getFormattedSlotTime(slotTime) {
    return this.global.getFormattedSlotTime(slotTime);
  }
  dateClicked(date) {
    console.log(date);
    this.selectedDate = date;
    this.selectedDateWithMonthName = this.global.AvailableSlotsFor(date.toString())
    this.getTechSlots(this.global.getCurrentDeviceDate(date.toString()))
    console.log('selectedSlotIndex ', this.selectedSlotIndex);
  }
  ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.navCtrl.pop();
    }, 10);
  }

}
