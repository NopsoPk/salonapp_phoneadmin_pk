import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DashboardPage } from './dashboard';
import { AppointmentDetailComponent } from '../../components/appointment-detail/appointment-detail';

@NgModule({
  declarations: [
    DashboardPage,
    AppointmentDetailComponent,
  ],
  imports: [
    IonicPageModule.forChild(DashboardPage),
  ],
})
export class DashboardPageModule {}
